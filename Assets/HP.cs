using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HP : MonoBehaviour
{
    public int maxHpAmount = 100;
    public int currentHP = 100;
    public Slider slider;

    public void TakeDamage(int damage)
    {
        currentHP -= damage;
        Die();
        updateHPBAR();
    }
    
    public void Die()
    {
        if (currentHP <= 0)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
    void updateHPBAR()
    {
        slider.value = currentHP;
    }
    void start()
    {
        slider.maxValue = maxHpAmount;
        slider.minValue = 0;
        updateHPBAR();
    }
}
