using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEnvironment : MonoBehaviour
{
    [SerializeField] int damage = 20;
    [SerializeField] LayerMask mask;
    [SerializeField] float attackRange = 2f;
    [SerializeField] float attackRate = 2f;
    float nextAttackTime = 0f;

    void Attack()
    {
        if (Time.time > nextAttackTime)
        {
        Collider2D cols = Physics2D.OverlapCircle(transform.position, attackRange, mask);
        try
        {
            if(cols!=null)
            {
                cols.GetComponent<HP>().TakeDamage(damage);
            }
        }
        catch
        {
            Debug.Log("Miss");
        }
        nextAttackTime = Time.time + 1f / attackRate;
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }

    void Update()
    {
        Attack();
    }
}
